import re
import sys


# Letters, numbers, or dashes
NAME_REGEX = r'^[a-zA-Z0-9-]+$'

name = '{{ cookiecutter.name }}'

if not re.match(NAME_REGEX, name):
    print('ERROR: {!r} does not conform to the naming pattern'.format(name))
    sys.exit(1)
