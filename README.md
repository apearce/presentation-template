# Slides template

A template repository for writing slides in Markdown, with conversion to PDF by 
pandoc, via LaTeX (beamer).

## Usage

Install the [cookiecutter][cookiecutter] Python package and then use it to
create a filled-in version of the slides.

    $ cookiecutter https://gitlab.cern.ch/apearce/presentation-template.git
    year [2019]:
    month [01]:
    day [08]:
    name [some-file-name]:
    title [The long title]:
    titleshort [The short title]:
    subtitle [The subtitle]:
    author [Alex Pearce]:
    location [A meeting]:
    url [https://indico.cern.ch/event/xxxxxx/]:

The created folder has a name as `year-month-day-name`, e.g.
`2019-01-08-some-file-name`.

To compile to document, run `make` inside the folder. The output will be placed
in the `build` directory.

[cookiecutter]: https://cookiecutter.readthedocs.io/en/latest/

## Background

The beamer [template][template] was generated with `pandoc -D beamer > 
template.beamer`, and then modified to suit my needs. The three `.sty` files 
are custom packages that I use for my projects.

[template]: https://github.com/jgm/pandoc-templates/blob/master/default.beamer
